## README

Angularを使ってユーザテーブルの一覧を表示します。
Angularとkaminariを使ってページングを実装しています。

サンプルの実行方法

* rake db:create

* rake db:migrate

* rake db:seed
  ユーザ100件作成

* rails s
  サーバ起動

簡単に解説

* views/users/index.html.slim

    ユーザの一覧を表示

    /users

    という形で叩かれます。

* app/views/users/index.json.jbuilder

    Angularのリソースから叩かれるjson
  
    /users.json
  
    という形で叩かれます。

* app/controllers/users_controller.rb

    indexアクションで、respond_toでhtmlとjsonで切り分けています。

    /usersが表示されると

    Angularによって動的にリストが生成されます。

* app/assets/javascripts/users.js.coffee

    Angularのコード
```
#!javascript
    angularExample.factory 'User', ['$resource', ($resource) ->
      return $resource 'users/:id.json', {}, {
        query: {method:'GET', params:{id: @id}, isArray:true},
        remove: {method: 'DELETE', params:{id: @id}, url: 'users/:id', withCredentials: true}}]
```


というところで、
/users.jsonへのアクセスをするリソースを定義しています。

* ページング

  html上で、ページネーションを表示するために、usersコントローラでは

    @users = User.all.page(params[:page]).per(10)

  app/views/users/index.html.slim
  
  では、
  
    = paginate @users

  という感じにしています。

  これで、Angularとは切り離された状態でページングが出来るようになります。
